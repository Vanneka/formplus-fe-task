## Getting Started with Formplus Task 📝

This is a task that takes into account

- Fetching from API's
- State Management
- Rendering responsive components


### To run
In order to run this application

1. `git clone` this repo into your local dev environment

2. `npm start` to start the application on your localhost